using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class VerticalListController : MonoBehaviour
{
    private GameObject[] games;
    public Sprite selectedSprite;
    public Sprite normalSprite;

    void Start()
    {
        games = new GameObject[10];

        GameObject buttonTemplate = transform.GetChild(0).gameObject;

        GameObject g;

        for(int i =1; i<=10; i++)
        {
            g = Instantiate(buttonTemplate, transform);

            g.transform.GetChild(0).GetComponent<Text>().text = "" + i;

            g.GetComponent <Button> ().AddEventListener (i, ItemClicked);

           games[i-1] = g;
        }

        Destroy(buttonTemplate);

    }


    void ItemClicked (int itemIndex)
	{
		//Debug.Log ("item : " + games[itemIndex-1].transform.GetChild(0).GetComponent<Text>().text + " clicked");

        for(int i = 0; i < 10; i++)
        {
            if(i==(itemIndex-1))
            {
		        games[i].GetComponent<Image>().sprite = selectedSprite;
                games[i].transform.GetChild(0).GetComponent<Text>().color = Color.white;
            }
            else 
            {
                games[i].GetComponent<Image>().sprite = normalSprite;

                games[i].transform.GetChild(0).GetComponent<Text>().color = Color.black;
            }
        }

        //games[itemIndex-1].GetComponent<Image>().sprite = selectedSprite;

	}
}



public static class ButtonExtension
{
	public static void AddEventListener<T> (this Button button, T param, Action<T> OnClick)
	{
		button.onClick.AddListener (delegate() {
			OnClick (param);
		});
	}
}
