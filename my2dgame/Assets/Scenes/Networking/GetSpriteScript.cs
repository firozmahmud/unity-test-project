using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class GetSpriteScript : MonoBehaviour
{

   MySprite[]  sprites;
   private Sprite demo;

   public GameObject game;

   private SpriteRenderer sr;

    void Start()
    {
        GameObject.Find("Get Sprite").GetComponent<Button>()
        .onClick.AddListener(GetSprites);

        //sr = gameObject.GetComponent<SpriteRenderer>();
    }



   public void GetSprites()
   {
        // StartCoroutine(GetGameObject());
        StartCoroutine(GetSprite());
   }

    [Serializable]	
    public struct MySprite
	{
		public string Name;
		public string Description;
		public Sprite Icon;
		public string IconUrl;
	}


    IEnumerator GetGameObject()
    {
        string url = "";
        
        UnityWebRequest req = UnityWebRequest.Get(url);
    
        req.chunkedTransfer = false;

        yield return req.Send();

        if(req.isNetworkError)
        {
            // No internet
        }
        else {
            if(req.isDone)
            {
             sprites =  JsonHelper.GetArray<MySprite> (req.downloadHandler.text);
             StartCoroutine(GetSprite());
            }
            else
           {
             // Failed to get data
           }
        }
    }


    IEnumerator GetSprite()
    {
        for(int i = 0; i < 1; i++)
        {
            //WWW w = new WWW(sprites[i].IconUrl);
            WWW w = new WWW("https://lms10.s3.ap-southeast-1.amazonaws.com/kids/hat.png");
            yield return w;

            if(w.error !=null)
            {
                // Show error 
                Debug.Log("No internet");
            }
            else 
            {
                if(w.isDone)
                {
                    Texture2D  tx = w.texture;

                    //sprites[i].Icon = Sprite.Create (tx, new Rect (0f, 0f, tx.width, tx.height), Vector2.zero, 10f);
                    demo = Sprite.Create (tx, new Rect (0f, 0f, tx.width, tx.height), Vector2.zero, 100f);
                    Debug.Log("Succuss");

                    ChangeSprite(demo);

                }
                else 
                {
                    // Failed 
                    Debug.Log("Failed");
                }
            }
        }
    }



    private void ChangeSprite(Sprite newSprite)
    {
        Instantiate(game, transform.position, transform.rotation);

         sr = gameObject.GetComponent<SpriteRenderer>();
         
        sr.sprite = newSprite;
    }
}
