using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class WebRequist : MonoBehaviour
{
    private InputField outputArea;  // Output area reference

    private void Start()
    {
        // Find the output field 
        outputArea = GameObject.Find("OutputArea").GetComponent<InputField>();

        // Add onClick Listener to the Get Data Button
        GameObject.Find("GetData").GetComponent<Button>().onClick.AddListener(GetData);

        // Add onClick Listener to the Post Data Button
        GameObject.Find("PostData").GetComponent<Button>().onClick.AddListener(PostData);
    }
    
    private void GetData() 
    {
        // This method called, when GetData button is Clicked
        StartCoroutine(GetData_Coroutine());
    }

    private void PostData()
    {
        // This method called, when PostData button is Clicked
        StartCoroutine(GetData_Coroutine());
    }

    private IEnumerator GetData_Coroutine() 
    {
        /**
        * This method is used to get data from the REST API
        */

        outputArea.text = "Loading...";
        string uri = "https://my-json-server.typicode.com/typicode/demo/posts";

        using(UnityWebRequest request = UnityWebRequest.Get(uri))
        {
           yield return request.SendWebRequest();

           if(request.isNetworkError || request.isHttpError)
           {
               outputArea.text = request.error;
           }
           else 
           {
               outputArea.text = request.downloadHandler.text;
           }
        }

    }


    private IEnumerator PostData_Coroutine()
    {
        /**
        * This method is used to post data to the REST API
        */

        outputArea.text = "Loading...";
        string uri = "https://my-json-server.typicode.com/typicode/demo/posts";

        WWWForm form = new WWWForm();
        form.AddField("title", "I am your data");

        using(UnityWebRequest request = UnityWebRequest.Post(uri, form))
        {
           yield return request.SendWebRequest();

           if(request.isNetworkError || request.isHttpError)
           {
               outputArea.text = request.downloadHandler.error;
           }
           else 
           {
               outputArea.text = request.downloadHandler.text;
           }
        }

    }
}
