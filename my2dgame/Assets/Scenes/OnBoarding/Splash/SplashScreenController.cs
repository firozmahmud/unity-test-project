using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SplashScreenController : MonoBehaviour
{

    void Start()
    {
        Invoke("LoadNextLevel" , 5);
    }

    private void LoadNextLevel()
    {
        SceneManager.LoadScene("LoginSignup");
    }

}
