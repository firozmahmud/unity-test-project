using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonClickListener : MonoBehaviour
{

    public void Login()
    {
        // Go to login scene
        SceneManager.LoadScene("LoginScene");
    }

    public void CreateAccount()
    {
        // Go to create account scene
    }
}
