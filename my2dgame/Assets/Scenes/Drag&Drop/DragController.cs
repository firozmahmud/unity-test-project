using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DragController : MonoBehaviour
{

   private Vector3 screenPoint;
   private Vector3 offset;
   private Rigidbody2D rb;
   private Vector3 screenBound;

void Start() 
{
  Screen.orientation = ScreenOrientation.LandscapeLeft;
  rb = gameObject.GetComponent<Rigidbody2D>() ;

  screenBound = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,
  Camera.main.transform.position.z));
}

void Update() 
{

  /*
  transform.position = new Vector3(
    Mathf.Clamp(transform.position.y, - 2f, 2f), 
    Mathf.Clamp(transform.position.x, -4f, 4f) , transform.position.z);

    */
}

/*
void LateUpdate() 
{
   Vector3 viewPos = transform.position;
   viewPos.x = Mathf.Clamp(viewPos.x, screenBound.x, screenBound.x * -1);
   viewPos.y = Mathf.Clamp(viewPos.y, screenBound.y, screenBound.y * -1);
   transform.position = viewPos;  
}
*/

  void OnMouseDown()
   {
    offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
   }
  
  void OnMouseDrag()
 {
    Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
    Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
   // transform.position = curPosition;
    rb.MovePosition(curPosition);
  }
}
