using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteResize : MonoBehaviour
{

   private Vector2 actualSize;
   private Vector2 targetSize;

   private bool isActive = false;

   private void Start() {
       
       actualSize = new Vector2(2.5f, 2f);
       targetSize = new Vector2(3.5f, 4f);

   }

    private void OnMouseDown()
    {
        StartCoroutine("ScaleObject");

    }


     IEnumerator ScaleObject()
  {
      float scaleDuration = 1;                                //animation duration in seconds
     // Vector3 actualScale = transform.localScale;             // scale of the object at the begining of the animation
      //Vector3 targetScale = new Vector3 (2f, 5f, 2f);     // scale of the object at the end of the animation
      
      for(float t = 0; t < 1; t += Time.deltaTime / scaleDuration )
      {
          if(isActive)
          {
              // Go back to previous size
              transform.localScale = Vector3.Lerp(targetSize ,actualSize ,t);
              yield return null;
          }
          else 
          {
              // Scale to taget size
              transform.localScale = Vector3.Lerp(actualSize ,targetSize ,t);
              yield return null;
          }

      }

      isActive = !isActive;
  }
}
