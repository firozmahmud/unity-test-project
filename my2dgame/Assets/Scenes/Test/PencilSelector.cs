using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PencilSelector : MonoBehaviour
{

    public GameObject flower;

    static string selectedColor = "red";

   Dictionary<string, Color> myList = new Dictionary<string, Color>();


   private void Start() {
       
       myList.Add("red", new Color(100,0,0) );
       myList.Add("green", new Color(0,191,0));
       myList.Add("blue", new Color(0,0,255));
       myList.Add("orange", new Color(243,176,13));
       myList.Add("yellow", new Color(255,140,0));
   }


   private void OnMouseDown()
   {
      
       switch(transform.gameObject.tag)
       {
           case "red":
           selectedColor = "red";
           break;

           case "green":
           selectedColor = "green";
           break;

           case "blue":
           selectedColor = "blue";
           break;

            case "orange":
           selectedColor = "orange";
           break; 


          case "yellow":
           selectedColor = "yellow";
           break;

           case "body":
           transform.gameObject.GetComponent<Renderer>().material.color = myList[selectedColor];
           break;

           case "dal":
           transform.gameObject.GetComponent<Renderer>().material.color = myList[selectedColor];
           break;

           case "leaf":
           transform.gameObject.GetComponent<Renderer>().material.color = myList[selectedColor];
           break;

           case "papri":
           transform.gameObject.GetComponent<Renderer>().material.color = myList[selectedColor];
           break;

       }
   }


}
