using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AppleController : MonoBehaviour 
{


    private void OnMouseDown()
    {
        switch(transform.gameObject.tag)
        {
            case "body":
            Debug.Log("Body");
            transform.gameObject.GetComponent<Renderer>().material.color = Color.green;
            break;

            case "dal":
            Debug.Log("Dal");
            transform.gameObject.GetComponent<Renderer>().material.color = Color.red;
            break;

            case "leaf":
            Debug.Log("Leaf");
            transform.gameObject.GetComponent<Renderer>().material.color = Color.blue;
            break;

            default:
            transform.gameObject.GetComponent<Renderer>().material.color = Color.yellow;
            break;
        }
    }

}
