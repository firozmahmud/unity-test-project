using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    public void BagIconClicked()
    {
        SceneManager.LoadScene("WatchHistory");
        // Debug.Log("Bag Clicked");
    }

    public void LibraryIconClicked()
    {
        SceneManager.LoadScene("KidsLibrary");
        // Debug.Log("Library Clicked");
    }
}
