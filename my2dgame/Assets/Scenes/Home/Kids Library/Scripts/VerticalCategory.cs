using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerticalCategory : MonoBehaviour
{
    public Sprite[] sprites;

    public Sprite selected;
    public Sprite unselected;

   private GameObject[] games;

   GridControllerInKids sn;

    void Start()
    {

        sn = GameObject.FindGameObjectWithTag("Hello").GetComponent<GridControllerInKids>();

       games = new GameObject[10];

        GameObject buttonTemplate = transform.GetChild(0).gameObject;

        GameObject g;

        for(int i =0; i<10; i++)
        {
            g = Instantiate(buttonTemplate, transform);

            //g.GetComponent<Image>().sprite = sprites[i];
            
            g.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Category " + i;

            g.GetComponent <Button> ().AddEventListener (i, ItemClicked);
           
           games[i] = g;
        }

        Destroy(buttonTemplate);
    }


      void ItemClicked (int itemIndex)
	{

        sn.PopulateGrid();

       for(int i = 0; i < 10; i++)
       {
           if(i == itemIndex)
           {
             games[i].GetComponent<Image>().sprite = selected;
           }
           else 
           {
               games[i].GetComponent<Image>().sprite = unselected;
           }
       }
	}
 
}
