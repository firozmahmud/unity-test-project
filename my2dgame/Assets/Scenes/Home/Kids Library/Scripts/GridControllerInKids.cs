using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridControllerInKids : MonoBehaviour
{
    public GameObject prefab;
    public int numberToCreate;

    public Sprite[] sprites;
    void Start()
    {
        PopulateGrid();
    }

    public void PopulateGrid()
    {
        GameObject newObj;

        for(int i = 0; i < numberToCreate; i++)
        {
            newObj = Instantiate(prefab, transform);
            newObj.GetComponent<Image>().sprite = sprites[i];
        }
    }
}
