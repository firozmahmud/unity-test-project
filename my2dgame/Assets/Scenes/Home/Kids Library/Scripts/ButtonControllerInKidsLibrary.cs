using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControllerInKidsLibrary : MonoBehaviour
{
    public void HomeIconClicked()
    {
        SceneManager.LoadScene("HomeScene");
    }

    public void ParentDashBoardIconClicked()
    {
        Debug.Log("Parent Dashboard Icon Clicked");
    }
}
